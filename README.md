# selenium4xray

##Configure the development environment

1. Mark directory 'src' as a 'Sources Root'
2. Set the Project Interpreter: Python 3.7
3. Install requirements.txt
4. Set the running configuration using the following:

Script path: <base-directory>/selenium4xray/src/selenium4xray/<script-name>
Parameters: -o ../out/results.json -b Chrome --driver ../webdrivers/chrome/mac64/chromedriver --device PC --window-size 1920x1480 --headless --no-sandbox --disable-popup-blocking --incognito --disable-dev-shm-usage
