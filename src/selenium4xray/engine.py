
import sys
import getopt
from selenium import webdriver
import datetime
import pytz
import json
import os
import importlib

TEST_EXECUTION = "test-execution"
OUTPUT = "output"
BROWSER = "browser"
DRIVER = "driver"
DEVICE = "device"
WINDOW_SIZE = "window-size"
HEADLESS = "headless"
NO_SANDBOX = "no-sandbox"
DISABLE_POPUP_BLOCKING = "disable-popup-blocking"
INCOGNITO = "incognito"
DISABLE_DEV_SHM_USAGE = "disable-dev-shm-usage"


class TestExecution:

    _configs = {
        OUTPUT: os.getcwd() + "/out/result.json",
        BROWSER: "Chrome",
        DRIVER: "local",
        DEVICE: "PC",
        WINDOW_SIZE: "1920x1480",
        HEADLESS: False,
        NO_SANDBOX: False,
        DISABLE_POPUP_BLOCKING: False,
        INCOGNITO: False
    }

    def __init__(self, summary, description, version, user, revision, test_plan_key, test_environments, configs):
        self._info = TestInfo(summary, description, version, user, revision, test_plan_key, test_environments)
        self._tests = []
        self._configs = configs

    def run(self):
        raise NotImplementedError

    def configs(self):
        return self._configs

    def start_execution(self):
        self._info.start_date()

    def finish_execution(self):
        self._info.finish_date()

    def save_test_result(self, test):
        self._tests.append(test)

    def to_json(self):
        return dict(info=self._info,
                    tests=self._tests)

    def results(self):

        json_data = json.dumps(self.to_json(), cls=ComplexEncoder)
        print(json_data)

        os.makedirs(os.path.dirname(self._configs[OUTPUT]), exist_ok=True)

        with open(self._configs[OUTPUT], 'w+') as fp:
            json.dump(self.to_json(), fp, cls=ComplexEncoder)


class TestInfo:

    LISBON = pytz.timezone('Europe/Lisbon')

    def __init__(self, summary, description, version, user, revision, test_plan_key, test_environments):
        self._summary = summary
        self._description = description
        self._version = version
        self._user = user
        self._revision = revision
        self._test_plan_key = test_plan_key
        self._test_environments = test_environments
        self._start_date = ""
        self._finish_date = ""

    def start_date(self):
        current_data_time = datetime.datetime.now(TestInfo.LISBON)
        current_data_time_formatted = current_data_time.strftime("%Y-%m-%dT%H:%M:%S%z")
        # custom UTC timezone separator
        self._start_date = current_data_time_formatted[0:22] + ":" + current_data_time_formatted[22:24]

    def finish_date(self):
        current_data_time = datetime.datetime.now(TestInfo.LISBON)
        current_data_time_formatted = current_data_time.strftime("%Y-%m-%dT%H:%M:%S%z")
        # custom UTC timezone separator
        self._finish_date = current_data_time_formatted[0:22] + ":" + current_data_time_formatted[22:24]

    def to_json(self):
        return dict(summary=self._summary,
                    description=self._description,
                    version=self._version,
                    user=self._user,
                    revision=self._revision,
                    testPlanKey=self._test_plan_key,
                    testEnvironments=self._test_environments,
                    startDate=self._start_date,
                    finishDate=self._finish_date)


class Test:

    LISBON = pytz.timezone('Europe/Lisbon')

    def __init__(self, test_key, configs):
        self.test_key = test_key
        self._start = ""
        self._finish = ""
        self._comment = ""
        self._status = ""
        self._steps = []
        self._configs = configs

    def test(self):
        raise NotImplementedError

    def configs(self):
        return self._configs

    def start(self):
        current_data_time = datetime.datetime.now(Test.LISBON)
        current_data_time_formatted = current_data_time.strftime("%Y-%m-%dT%H:%M:%S%z")
        # custom UTC timezone separator
        self._start = current_data_time_formatted[0:22] + ":" + current_data_time_formatted[22:24]

    def finish(self):
        current_data_time = datetime.datetime.now(Test.LISBON)
        current_data_time_formatted = current_data_time.strftime("%Y-%m-%dT%H:%M:%S%z")
        # custom UTC timezone separator
        self._finish = current_data_time_formatted[0:22] + ":" + current_data_time_formatted[22:24]

    def append_step(self, step):
        self._steps.append(step)

    def status(self, status):
        self._status = status

    def comment(self, comment):
        self._comment = comment

    def to_json(self):
        return dict(testKey=self.test_key,
                    start=self._start,
                    finish=self._finish,
                    comment=self._comment,
                    status=self._status,
                    steps=self._steps)


class TestStep:

    def __init__(self):
        self._status = ""
        self._comment = ""

    def status(self, status):
        self._status = status

    def comment(self, comment):
        self._comment = comment

    def to_json(self):
        return dict(comment=self._comment,
                    status=self._status)


class ComplexEncoder(json.JSONEncoder):

    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return json.JSONEncoder.default(self, obj)


class WebDriverWrapper:

    def __init__(self, configs):
        self._options = None
        self._webdriverwrapper = None
        self._configs = configs

    def open(self):

        if self._configs[BROWSER] == "Chrome":
            self._options = webdriver.ChromeOptions()
        elif self._configs[BROWSER] == "IE":
            # self._options = webdriver.IeOptions()
            raise Exception("Unsupported browser")
        elif self._configs[BROWSER] == "Firefox":
            # self._options = webdriver.FirefoxOptions()
            raise Exception("Unsupported browser")
        elif self._configs[BROWSER] == "Safari":
            raise Exception("Unsupported browser")
        else:
            raise Exception("Unknown browser")

        if self._configs[DEVICE] == "PC":
            pass
        elif self._configs[DEVICE] == "MAC":
            pass
        elif self._configs[DEVICE] == "iPhone-5":
            mobile_emulation = {"deviceName": "iPhone 5"}
            self._options.add_experimental_option("mobileEmulation", mobile_emulation)
        elif self._configs[DEVICE] == "Nexus-5":
            mobile_emulation = {"deviceName": "Nexus 5"}
            self._options.add_experimental_option("mobileEmulation", mobile_emulation)
        else:
            raise Exception("Unknown device")

        self._options.add_argument('window-size=' + self._configs[WINDOW_SIZE])

        if self._configs[HEADLESS]:
            self._options.add_argument('--headless')

        if self._configs[NO_SANDBOX]:
            self._options.add_argument('--no-sandbox')

        if self._configs[DISABLE_POPUP_BLOCKING]:
            self._options.add_argument('--disable-popup-blocking')

        if self._configs[INCOGNITO]:
            self._options.add_argument('--incognito')

        if self._configs[DISABLE_DEV_SHM_USAGE]:
            self._options.add_argument("--disable-dev-shm-usage")

        if self._configs[DRIVER] == "global":
            if self._configs[BROWSER] == "Chrome":
                self._webdriverwrapper = webdriver.Chrome(options=self._options)
            elif self._configs[BROWSER] == "IE":
                raise Exception("Unsupported browser")
            elif self._configs[BROWSER] == "Firefox":
                #self._webdriverwrapper = webdriver.Firefox(options=self._options)
                raise Exception("Unsupported browser")
            elif self._configs[BROWSER] == "Safari":
                raise Exception("Unsupported browser")
            else:
                raise Exception("Unknown browser")
        else:
            if self._configs[BROWSER] == "Chrome":
                self._webdriverwrapper = webdriver.Chrome(options=self._options, executable_path=self._configs[DRIVER])
            elif self._configs[BROWSER] == "IE":
                raise Exception("Unsupported browser")
            elif self._configs[BROWSER] == "Firefox":
                # self._webdriverwrapper = webdriver.Firefox(options=self._options, executable_path=self._configs[DRIVER])
                raise Exception("Unsupported browser")
            elif self._configs[BROWSER] == "Safari":
                raise Exception("Unsupported browser")
            else:
                raise Exception("Unknown browser")

        return self._webdriverwrapper

    def close(self):
        self._webdriverwrapper.quit()


def launcher(module_name, class_name, argv):

    try:

        configs = {
            OUTPUT: os.getcwd() + "/out/result.json",
            BROWSER: "Chrome",
            DRIVER: "local",
            DEVICE: "PC",
            WINDOW_SIZE: "1920x1480",
            HEADLESS: False,
            NO_SANDBOX: False,
            DISABLE_POPUP_BLOCKING: False,
            INCOGNITO: False,
            DISABLE_DEV_SHM_USAGE: True
        }

        # print('argv      :', sys.argv[1:])
        opts, args = getopt.getopt(sys.argv[1:], "h:o:b:", ["help",
                                                             OUTPUT + "=",
                                                             BROWSER + "=",
                                                             DRIVER + "=",
                                                             DEVICE + "=",
                                                             WINDOW_SIZE + "=",
                                                             HEADLESS,
                                                             NO_SANDBOX,
                                                             DISABLE_POPUP_BLOCKING,
                                                             INCOGNITO,
                                                             DISABLE_DEV_SHM_USAGE])

        # print('opts   :', opts)

        for opt, arg in opts:
            if opt in ("-o", "--"+OUTPUT):
                configs[OUTPUT] = arg
            elif opt in ("-b", "--"+BROWSER):
                configs[BROWSER] = arg
            elif opt == "--" + DRIVER:
                configs[DRIVER] = arg
            elif opt == "--" + DEVICE:
                configs[DEVICE] = arg
            elif opt == "--" + WINDOW_SIZE:
                configs[WINDOW_SIZE] = arg
            elif opt == "--" + HEADLESS:
                configs[HEADLESS] = True
            elif opt == "--" + NO_SANDBOX:
                configs[NO_SANDBOX] = True
            elif opt == "--" + DISABLE_POPUP_BLOCKING:
                configs[DISABLE_POPUP_BLOCKING] = True
            elif opt == "--" + INCOGNITO:
                configs[INCOGNITO] = True
            elif opt == "--" + DISABLE_DEV_SHM_USAGE:
                configs[DISABLE_DEV_SHM_USAGE] = True
            else:
                usage()

        module = importlib.import_module(module_name)
        class_ = getattr(module, class_name)
        instance = class_(configs)
        instance.run()

    except getopt.GetoptError as err:
        # print(str(err))
        usage()
        sys.exit()


def usage():
    print("Usage:	launcher.py [OPTIONS]")
    print("Options:")
    print(" -x, " + TEST_EXECUTION + " string" +
          "\n -o, " + OUTPUT + " string" +
          "\n -b, " + BROWSER + " string" +
          "\n --" + DRIVER + " string" +
          "\n --" + DEVICE + " string" +
          "\n --" + WINDOW_SIZE + " string" +
          "\n --" + HEADLESS +
          "\n --" + NO_SANDBOX +
          "\n --" + DISABLE_POPUP_BLOCKING +
          "\n --" + INCOGNITO +
          "\n --" + DISABLE_DEV_SHM_USAGE)


